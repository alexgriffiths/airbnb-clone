import { useState } from 'react'
import ReactMapGL, { Marker } from 'react-map-gl'
import { getCenter } from 'geolib'
import "mapbox-gl/dist/mapbox-gl.css"

function Map({ searchResults }) {

  const coordinates = searchResults.map((result) => ({
    longitude: result.long,
    latitude: result.lat
  }))
  
  const center = getCenter(coordinates)

  const [viewport, setViewport] = useState({
    width: '100%',
    height: '100%',
    latitude: center.latitude,
    longitude: center.longitude,
    zoom: 11
  })

  return (
    <ReactMapGL
      {...viewport}
      mapStyle={'mapbox://styles/asjdf32/cl1fiu9ec00hd14o3bseo6rvr'}
      mapboxAccessToken={process.env.mapbox_key}
      onMove={evt => setViewport(evt.viewport)}
    >

      {searchResults.map((result) => {
        return (
          <div key={result.long}>
            <Marker
              longitude={result.long}
              latitude={result.lat}
              offsetLeft={-20}
              offsetTop={-10}
            >
              <p className='cursor-pointer text-2xl animate-bounce'>
              📌 
              </p>
            </Marker>
          </div>
        )
      })}
    </ReactMapGL>
  )
}

export default Map